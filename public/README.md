# User Stories
### As a user, I can learn information about you.
- I can see your full name.
- I can read a short description of you.
- (Optional) I can see a photo of you.
### As a user, I can learn about your skills.
- I can click a link to download your resume.
- I can see a list of technologies and stacks that you use. Each technology and stack should be represented by an image icon/logo with an alt text.
### As a user, I can see how to contact or follow you.
- I can click a link to open a new email using my default email client.
- I can click a link to visit your GitHub or Gitlab page.
- I can click a link to visit your LinkedIn page.
### As a user, I can navigate your site.
- There is a navigation UI that has links to all of the pages or sections.
- When I click on a link in the navigation UI, I can view that page/section.
- The navigation should appear in a consistent place across pages and is easy to use.
### As a user, I can experience an interactive UI.
- I can experience the website on mobile, tablet and desktop screens.
- No image stretching.
- No information or error logs are seen on the browser's console.
- No dead links.

# Technical Specifications
- Code should have both grid AND flexbox implementation.
- Use of pure CSS, no libraries or frameworks.
- Use of color contrast that is accessible.
- Use of 2-3 font variations.
- Use of Uplift Code Camp's HTML and CSS coding standards.
- Use of Uplift Code Camp's committing standards.